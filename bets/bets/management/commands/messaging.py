from django.conf import settings
from django.core.management.base import BaseCommand
from steamauth.models import SteamUser
from kombu import BrokerConnection
from kombu.messaging import Exchange, Queue, Consumer

"""
web.hello

Bot saying hi.

{"source": bot_id}
"""
def hello(body, message):
	print "Bot " + str(body['source']) + " says hello."
	message.ack()

"""
web.response

Response to challenge sent by web

{"steamid": "SteamID64", "value": "response value", "source": bot_id}
"""
def response(body, message):
	print body['steamid'], 'says:', body['value']
	try:
		user = SteamUser.objects.get(steamid=int(body['steamid']))
	except SteamUser.DoesNotExist:
		return False
	user.response = body['value']
	user.save()
	message.ack()

task_map = {
	'web.hello': hello,
	'web.response': response,
}

def task_dispatch(body, message):
	try:
		fun = task_map[message.delivery_info['routing_key']]
	except KeyError:
		print "Failed to load task for '" + message.delivery_info['routing_key'] + "'"
		message.reject()
		return
	try:
		fun(body, message)
	except Exception as e:
		print "Failed to excecute task for '" + message.delivery_info['routing_key'] + "':", e
		message.reject()


class Command(BaseCommand):
	help = 'Handles incoming AMQP messages'

	def handle(self, *args, **options):
		with BrokerConnection(getattr(settings, 'BROKER_URL', None)) as connection:
			channel = connection.channel()
			exchange = Exchange("web", "topic", durable=True)
			queue = Queue("webq", exchange=exchange, routing_key="web.#")

			while True:
				with Consumer(channel, queue, callbacks=[task_dispatch]) as consumer:
					connection.drain_events()