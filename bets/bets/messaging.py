from django.conf import settings
from kombu import BrokerConnection, Exchange
from kombu.common import maybe_declare
from kombu.pools import producers


def publish(routing_key, botid, message):
	exchange = Exchange("bot", "topic", durable=True)

	connection = BrokerConnection(getattr(settings, 'BROKER_URL', None))

	with producers[connection].acquire(block=True) as producer:
		maybe_declare(exchange, producer.channel)
		producer.publish(message, routing_key=routing_key.format(bot=botid),
			serializer="json", exchange=exchange)


def challenge(botid, steamid, previous_response):
	publish('bot.{bot}.challenge', botid,
		{'steamid': str(steamid), 'previous_response': previous_response})
