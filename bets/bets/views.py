from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from bets.messaging import challenge as challenge_msg


def home(request):
	return render(request, 'home.html')


def challenge(request):
	challenge_msg(
		botid=1, # yeah.
		steamid=request.user.steamid,
		previous_response=request.user.response,
	)
	messages.success(request, "Ze bot will process your request")
	return redirect(reverse('home'))
