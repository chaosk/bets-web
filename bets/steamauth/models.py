from django.core.urlresolvers import reverse
from django.db import models
from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from steamauth.tasks import update_profiles


STEAMCOMMUNITY_URL = "http://steamcommunity.com"


class SteamUserManager(BaseUserManager):

	def create_user(self, steamid):
		"""Creates and saves a SteamUser with the given steamid"""
		if not steamid:
			raise ValueError("User must have a steamid64")

		user = self.model(
			steamid=steamid
		)
		user.save(using=self._db)
		return user

	def create_superuser(self, steamid):
		"""
		Creates and saves a SteamUser with
		admin privileges and given steamid
		"""
		if not steamid:
			raise ValueError("User must have a steamid64")

		user = self.model(
			steamid=steamid,
			level=SteamUser.LEVEL_SUPERHERO
		)
		user.save(using=self._db)
		return user


class SteamUser(AbstractBaseUser):
	"""Model for Steam OpenID authentication."""

	"""Holds steamid64 received from a successful login attempt."""
	steamid = models.BigIntegerField(unique=True, db_index=True,
		verbose_name="SteamID64")

	"""Stores the date user logged in the very first time."""
	created_at = models.DateTimeField(auto_now_add=True)

	"""Stores username retrieved from Steam."""
	username = models.CharField(max_length=100, blank=True)

	"""Stores Vanity URL, optional."""
	profile_url = models.CharField(max_length=255, blank=True)

	"""Stores URL to 32x32 user avatar."""
	avatar = models.CharField(max_length=255, blank=True)

	"""Stores URL to 184x184 user avatar."""
	avatar_full = models.CharField(max_length=255, blank=True)

	response = models.CharField(max_length=255, blank=True)

	is_active = models.BooleanField(default=True)

	is_staff = models.BooleanField(default=False)

	objects = SteamUserManager()

	USERNAME_FIELD = 'steamid'
	REQUIRED_FIELDS = []

	class Meta:
		ordering = ['created_at']

	def get_absolute_url(self):
		return reverse('user_details',
			args=[str(self.steamid)])

	def get_full_name(self):
		return self.username or unicode(self.steamid)

	def get_short_name(self):
		return self.username or unicode(self.steamid)

	def __unicode__(self):
		return self.username or unicode(self.steamid)

	def has_perm(self, perm, obj=None):
		return True

	def has_module_perm(self, app_label):
		return True

	@property
	def vanity_url(self):
		return self.profile_url[len(STEAMCOMMUNITY_URL):]


class Nonce(models.Model):
	server_url = models.CharField(max_length=2047)
	timestamp = models.IntegerField()
	salt = models.CharField(max_length=40)

	def __unicode__(self):
		return u"Nonce: %s, %s" % (self.server_url, self.salt)


class Association(models.Model):
	server_url = models.TextField(max_length=2047)
	handle = models.CharField(max_length=255)
	secret = models.TextField(max_length=255) # Stored base64 encoded
	issued = models.IntegerField()
	lifetime = models.IntegerField()
	assoc_type = models.TextField(max_length=64)

	def __unicode__(self):
		return u"Association: %s, %s" % (self.server_url, self.handle)


@receiver(user_logged_in)
def update_profile_signal(sender, **kwargs):
	#update_profiles.delay((kwargs['user'].id,))
	update_profiles((kwargs['user'].id,))
